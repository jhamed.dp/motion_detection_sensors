import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

df = pd.read_csv('/home/parstech/Hamed/sensor_matt/21061501-v2.csv')   # 21061510_v2    21061501-v2
print(df[['s1', 's2', 's3', 's4']].describe())

df2 = df[df['s1'] > df['s1'].mean()]
# sns.lineplot(data=df[['s1', 's2', 's3', 's4']])
sns.lineplot(data=df2[['s1', 's2', 's3', 's4']])
plt.show()
