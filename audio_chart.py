from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from PySide6.QtCharts import *
from worker import LoadAudio
from chart_widget import ChartWidget


class AudioChart(QWidget):
    def __init__(self, audio_path, icon_path=''):
        super(AudioChart, self).__init__()
        self.size = QSize(50, 50)
        self.icon_path = icon_path
        self.icon_pixmap = QPixmap(self.icon_path).scaled(self.size)
        self.icon_label = QLabel()
        self.icon_label.setPixmap(self.icon_pixmap)
        self.icon_label.setMaximumSize(self.size)

        self.chart = QChart()
        self.series = QLineSeries()
        self.chart.addSeries(self.series)
        self.chart.createDefaultAxes()
        self.chart.legend().hide()
        self.chart.setTitle("Sensor 4")

        self.load_data = LoadAudio(audio_path, column=3)
        self.load_data.result.connect(self.add_series)
        self.load_data.start()

        # Creating QChartView
        self.chart_view = QChartView(self.chart)
        self.chart_view.setRenderHint(QPainter.Antialiasing)

        # QWidget Layout
        self.main_layout = QHBoxLayout()
        size = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        # Center Layout
        self.chart_view.setSizePolicy(size)
        self.main_layout.addWidget(self.icon_label, 1)
        self.main_layout.addWidget(self.chart_view, 10)
        self.main_layout.setContentsMargins(0, 0, 0, 0)

        # Set the layout to the QWidget
        self.setLayout(self.main_layout)
        self.setContentsMargins(0, 0, 0, 0)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

    def add_series(self, x, y):
        self.chart.removeSeries(self.series)
        self.series.append(QPointF(x, y))
        # print(self.series.__sizeof__())
        # print(len(self.series.pointsVector()))
        if len(self.series.pointsVector()) > 400:
            # self.series.pointsVector().pop(0)
            self.series.remove(0)
        self.chart.addSeries(self.series)


class AudioChartLayout(QWidget):
    def __init__(self):
        super(AudioChartLayout, self).__init__()
        self.audio_widget = AudioChart('sensors-data.csv', 'images/sensor1.png')
        self.chart_widget3 = ChartWidget('sensors-data.csv', 'Sensor 4', 'images/sensor1.png', column=3)

        # Charts layout
        self.chart_layout = QGridLayout()
        self.chart_layout.addWidget(self.audio_widget)
        self.chart_layout.setSpacing(0)
        self.setLayout(self.chart_layout)
        self.setContentsMargins(0, 0, 0, 0)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
