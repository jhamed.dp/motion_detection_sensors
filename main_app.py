import sys
import time
import cv2
import pandas
from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import QGraphicsItemAnimation
from chart_widget import ChartLayout
from audio_chart import AudioChartLayout
from status import MultipleStatus, AudioStatus
from logo import SetLogo
from PySide6.QtUiTools import QUiLoader
from statistics import mean, median, stdev
import seaborn as sns
import matplotlib.pyplot as plt


class MainWindow(QMainWindow):
    def __init__(self, path):
        super().__init__()
        self.path = path
        self.df = pandas.read_csv(self.path, index_col='dv_id')
        self.df = self.df[self.df['s1'] > self.df['s1'].mean()]

        # sns.lineplot(data=self.df[['s1', 's2', 's3', 's4']])
        # plt.show()

        loader = QUiLoader()
        self.ui = loader.load('form.ui')

        # video process
        self.cap = cv2.VideoCapture('/home/parstech/Hamed/sensor_matt/510.mp4')
        fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.show_frames_timer = QTimer()
        self.show_frames_timer.timeout.connect(self.show_video)
        self.show_frames_timer.start(round(1000/fps))

        # self.audio_layout = AudioChartLayout()
        # self.audio_status = AudioStatus()
        # self.ui.frame_status.layout().addWidget(self.audio_status)

        # frame: QFrame = self.ui.frame_logo

        # self.logo = SetLogo()
        # self.ui.frame_logo.layout().addWidget(self.logo)
        # frame.layout().addWidget(self.logo)

        self.chart_layout = ChartLayout()
        self.ui.frame_plot.layout().addWidget(self.chart_layout)

        self.ui.show()

        # Exit QAction
        exit_action = QAction("Exit", self)
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exit_app)
        # self.file_menu.addAction(exit_action)

        self.status_timer = QTimer()
        # self.status_timer.timeout.connect(self.check_status_2)
        # self.status_timer.start(200)

        self.std_state1 = False
        self.std_state2 = False
        self.std_state3 = False
        self.std_state4 = False

    def show_video(self):
        ret, frame = self.cap.read()
        if ret:
            qt_frame = self.convert_arr_image_2_qt(frame, 260)
            # pixmap = QPixmap(frame)
            self.ui.label.setPixmap(qt_frame)

    def convert_arr_image_2_qt(self, cv_img, size):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
        p = convert_to_qt_format.scaledToWidth(size, Qt.FastTransformation)
        return QPixmap.fromImage(p)

    @Slot()
    def exit_app(self):
        sys.exit()

    def check_status_2(self):
        try:
            motion1 = self.chart_layout.chart_widget.series.pointsVector()[-20:]  # S1
            motion2 = self.chart_layout.chart_widget1.series.pointsVector()[-20:]  # S2
            motion3 = self.chart_layout.chart_widget2.series.pointsVector()[-20:]  # S3
            motion4 = self.audio_layout.audio_widget.series.pointsVector()[-20:]  # S4

            # ---------------------
            s1_mean = self.df.describe().loc['mean'][0]
            s2_mean = self.df.describe().loc['mean'][1]
            s3_mean = self.df.describe().loc['mean'][2]
            s4_mean = self.df.describe().loc['mean'][3]

            med1 = median([item.y() for item in motion1])
            med2 = median([item.y() for item in motion2])
            med3 = median([item.y() for item in motion3])
            med4 = median([item.y() for item in motion4])

            std1 = stdev([item.y() for item in motion1])
            std2 = stdev([item.y() for item in motion2])
            std3 = stdev([item.y() for item in motion3])
            std4 = stdev([item.y() for item in motion4])

            if std1 > 6 and not self.std_state1:
                self.std_state1 = True
            elif std1 > 6 and self.std_state1:
                self.std_state1 = False

            if std2 > 6 and not self.std_state2:
                self.std_state2 = True
            elif std2 > 6 and self.std_state2:
                self.std_state2 = False

            if std3 > 6 and not self.std_state3:
                self.std_state3 = True
            elif std3 > 6 and self.std_state3:
                self.std_state3 = False

            if std4 > 6 and not self.std_state4:
                self.std_state4 = True
            elif std4 > 6 and self.std_state4:
                self.std_state4 = False

            # ---------------------
            # if med1 > s1_mean and med4 > s4_mean:
            #     status_person = 2
            # elif med1 > s1_mean and med3 > s3_mean:
            #     status_person = 1
            # elif med3 > s3_mean and med2 > s2_mean:
            #     status_person = 3
            # elif med2 > s2_mean and med4 > s4_mean:
            #     status_person = 0
            # else:
            #     status_person = 4
            # ---------------------
            if std1 and std4:
                status_person = 2
            elif std1 and std3:
                status_person = 1
            elif std3 and std2:
                status_person = 3
            elif std2 and std4:
                status_person = 0
            else:
                status_person = 4

            # print(median([item.y() for item in motion1]))
            # print(median([item.y() for item in motion2]))
            # print(median([item.y() for item in motion3]))
            # print(median([item.y() for item in motion4]))
            print(status_person)
            # print(stdev([item.y() for item in motion1]))
            print("#########")
        except Exception as e:
            print("error: " + str(e))
            status_person = 4

        # if status_person == 3:
        #     self.logo.update('logo/joy_left.jpg')
        # elif status_person == 2:
        #     self.logo.update('logo/joy_right.jpg')
        # elif status_person == 1:
        #     self.logo.update('logo/joy_down.jpg')
        # elif status_person == 0:
        #     self.logo.update('logo/joy_up.jpg')
        # else:
        #     self.logo.update('logo/joy.jpg')

        # chooser = 0
        # for widget in (self.audio_status.children()):
        #     # print(status_person)
        #     if type(widget) == QLabel:
        #         if chooser == status_person:
        #             # self.status_bar.empty_label_img.setStyleSheet('')
        #             widget.setStyleSheet('background-color:#76FF03')
        #             # self.status_bar.safe_label_img.setStyleSheet('background-color:#76FF03')
        #         else:
        #             widget.setStyleSheet('background-color:#A7C0CD')
        #
        #         chooser +=1


if __name__ == '__main__':
    input_file_path = '/home/parstech/Hamed/sensor_matt/21061510_v3.csv'
    app = QApplication(sys.argv)
    window = MainWindow(input_file_path)
    # window.show()
    sys.exit(app.exec())
