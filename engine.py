import os
import numpy as np
import pandas as pd
from glob import glob
import json
from tqdm import tqdm


class ReadData:
    def __init__(self, path):
        self.path = path
        '''motion data'''
        self.motion_path = os.path.join(self.path, 'Motion_Data')
        self.motion_json = glob(os.path.join(self.motion_path, '*.json'))
        self.motion_data = self.open_motion_data()
        self.list_df = self.extract_values()

        '''audio data'''
        self.audio_path = os.path.join(self.path, 'Audio_Data')
        self.audio_files = glob(os.path.join(self.audio_path, '*', '*.wav'))

    def open_motion_data(self):
        data_dict = []
        for f in self.motion_json:
            # name = os.path.basename(f)
            # partition = name.split('_')[0]
            with open(f, 'r') as open_file:
                data = json.load(open_file)[0]

            data_dict.append(data)

        return data_dict

    def extract_values(self):
        df_list = []
        for ii, _data_ in tqdm(enumerate(self.motion_data)):
            df = pd.DataFrame(columns=['Date', 'Value'])
            for i, entry in enumerate(_data_['entries']):
                value = entry['value']
                date = entry['measurementDate']
                df.loc[i] = [date, value]
            df.to_csv(str(ii) + '.csv')
            df_list.append(df)
        return df_list


if __name__ == '__main__':
    read_data = ReadData('data')
    print()
