import torch
from PySide6.QtCore import *
from dateutil.parser import parse
import numpy as np
import pandas as pd
import traceback, sys
import time
import torchaudio
from torchaudio import transforms


class WorkerSignal(QObject):
    """
    Defines the signals available from a running worker thread
    Supported signals are:

    finished
        No data
    error
        tuple (exctype, value, traceback.format_exc() )
    result
        object data returned from processing, anything
    progress
    int indicating % progress
    """
    finished = Signal()
    error = Signal(tuple)
    result = Signal(object)
    progress = Signal(int)


class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        """
        Worker thread

        Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

        :param fn: the function callback to run on this worker thread. Supplied args and kwargs
                    will be passed through to the runner.
        :type fn: py_function
        :param args:  Arguments to pass to the callback function
        :param kwargs: Keywords to pass to the callback function.
        """
        super(Worker, self).__init__()

        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignal()

        # Add the callback to our kwargs
        # self.kwargs['progress_callback'] = self.signals.progress

    @Slot()
    def run(self):
        """
        Initialise the runner function with passed args, kwargs.
        :return:
        """

        # Retrieve args/kwargs here and fire processing using them.
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing.
        finally:
            self.signals.finished.emit()


def transform_date(utc, timezone=None):
    utc = parse(utc)
    utc = utc.date().isoformat() + 'T' + utc.time().isoformat()
    utc_fmt = "yyyy-MM-ddTHH:mm:ss"
    new_date = QDateTime().fromString(utc, utc_fmt)
    if timezone:
        new_date.setTimeZone(timezone)
    return new_date


def read_data(fname):
    df = pd.read_csv(fname)
    values = df['Value'].values
    timezone = QTimeZone(b"Europe/Berlin")

    times = df['Date'].apply(lambda x: transform_date(x, timezone))
    times = times.values

    return np.c_[times, values]


class LoadData(QThread):
    finished = Signal()
    error = Signal(tuple)
    result = Signal(object, object)
    progress = Signal(int)

    def __init__(self, path):
        super(LoadData, self).__init__()
        self.path = path

    @Slot()
    def run(self):
        data = read_data(self.path)
        date_fmt = "yyyy-MM-dd HH:mm:ss"
        # Filling QLineSeries
        for i in range(len(data)):
            # Getting the data
            t = data[i, 0]
            t = '{}'.format(t.toPython())
            x = QDateTime().fromString(t, date_fmt).toMSecsSinceEpoch()
            y = data[i, 1]

            self.result.emit(x, y)
            time.sleep(0.5)


class LoadAudio(QThread):
    result = Signal(object, object)

    def __init__(self, path, column):
        super(LoadAudio, self).__init__()
        self.path = path
        self.column = column
        self.csv_file = pd.read_csv(self.path, index_col='hours')  # torchaudio.load(self.path)
        # sample_rate = self.csv_file[1]
        # self.tfms = transforms.Resample(sample_rate, 1024)

    @Slot()
    def run(self):
        # sound_data = self.tfms(self.csv_file[0][0])
        # sound_data = sound_data[torch.abs(sound_data) > 0.05]
        for i in range(self.csv_file.index.size):
            time_ = i  # self.csv_file.index[i]
            signal = self.csv_file[self.csv_file.keys()[self.column]][i]
            self.result.emit(time_, signal)
            time.sleep(0.0148)
