import sys

import pandas
from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import QGraphicsItemAnimation
from chart_widget import ChartLayout
from audio_chart import AudioChartLayout
from status import MultipleStatus, AudioStatus
from logo import SetLogo
from statistics import mean, median


class MainWindow(QMainWindow):
    def __init__(self, path):
        super().__init__()
        self.size = QSize(800, 800)
        self.setWindowTitle("MultiModal Demo")
        self.setMinimumSize(self.size)
        self.path = path

        self.df = pandas.read_csv(self.path, index_col='hours')

        # Menu
        self.menu = self.menuBar()
        # self.file_menu = self.menu.addMenu("File")

        # Exit QAction
        exit_action = QAction("Exit", self)
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exit_app)
        # self.file_menu.addAction(exit_action)

        self.chart_layout = ChartLayout()
        self.audio_layout = AudioChartLayout()
        # self.status_bar = MultipleStatus()
        self.audio_status = AudioStatus()
        # self.status_bar.setMaximumHeight(100)
        # self.status_bar.setContentsMargins(0, 0, 0, 0)
        self.logo = SetLogo()

        _widget = QWidget()
        _v_layout_left = QVBoxLayout()
        _v_layout_right = QVBoxLayout()
        layout = QHBoxLayout()

        # _v_layout_left.addWidget(self.status_bar, 1)
        _v_layout_left.addWidget(self.chart_layout, 3)
        # _v_layout_left.addWidget(self.audio_layout, 2)

        _v_layout_right.addWidget(self.audio_status, 4)
        _v_layout_right.addWidget(self.logo, 1)

        layout.addLayout(_v_layout_left, 9)
        layout.addLayout(_v_layout_right, 1)
        _widget.setLayout(layout)
        self.setCentralWidget(_widget)

        self.status_timer = QTimer()
        self.status_timer.timeout.connect(self.check_status_2)
        self.status_timer.start(200)

        self.audio_tick = False
        self.motion_tick = False
        self.motion_empty_tick = False

    def check_status(self):
        self.status_bar.status1.setStyleSheet('')
        self.status_bar.status2.setStyleSheet('')
        motion = self.chart_layout.chart_widget.series.pointsVector()[-1]
        audio_signal = self.audio_layout.audio_widget.series.pointsVector()[-1]

        bandwidth = len(self.audio_status.classes) *2
        audio_signal_y = audio_signal.y()
        int_audio_signal_y = abs(int(audio_signal_y * bandwidth))
        print(audio_signal_y)

        if motion.y() < 0:
            self.status_bar.status1.setStyleSheet('background-color:#76FF03')
            chooser = 0
            for widget in self.audio_status.children():
                if type(widget) == QLabel:
                    if chooser == int_audio_signal_y:
                        widget.setStyleSheet('background-color:#D4E157')
                    else:
                        widget.setStyleSheet('background-color:#A7C0CD')

                    chooser += 1
        else:
            self.status_bar.status2.setStyleSheet('background-color:#FFFF00')
            for widget in self.audio_status.children():
                if type(widget) == QLabel:
                    widget.setStyleSheet('background-color:#A7C0CD')


    def check_status_2(self):
        try:
            motion1 = self.chart_layout.chart_widget.series.pointsVector()[-20:]  # S1
            motion2 = self.chart_layout.chart_widget1.series.pointsVector()[-20:]  # S2
            motion3 = self.chart_layout.chart_widget2.series.pointsVector()[-20:]  # S3
            motion4 = self.audio_layout.audio_widget.series.pointsVector()[-20:]  # S4

            # ---------------------
            s1_mean = self.df.describe().loc['mean'][0]
            s2_mean = self.df.describe().loc['mean'][1]
            s3_mean = self.df.describe().loc['mean'][2]
            s4_mean = self.df.describe().loc['mean'][3]

            med1 = median([item.y() for item in motion1])
            med2 = median([item.y() for item in motion2])
            med3 = median([item.y() for item in motion3])
            med4 = median([item.y() for item in motion4])
            # ---------------------
            if med1 > s1_mean and med4 > s4_mean:
                status_person = 2
            elif med1 > s1_mean and med3 > s3_mean:
                status_person = 1
            elif med3 > s3_mean and med2 > s2_mean:
                status_person = 3
            elif med2 > s2_mean and med4 > s4_mean:
                status_person = 0
            else:
                status_person = 4

            print(median([item.y() for item in motion1]))
            print(median([item.y() for item in motion2]))
            print(median([item.y() for item in motion3]))
            print(median([item.y() for item in motion4]))
            print("#########")
        except Exception as e:
            print("error: " + str(e))
            status_person = 4

        if status_person == 3:
            self.logo.update('logo/joy_left.jpg')
        elif status_person == 2:
            self.logo.update('logo/joy_right.jpg')
        elif status_person == 1:
            self.logo.update('logo/joy_down.jpg')
        elif status_person == 0:
            self.logo.update('logo/joy_up.jpg')
        else:
            self.logo.update('logo/joy.jpg')

        chooser = 0
        for widget in (self.audio_status.children()):
            # print(status_person)
            if type(widget) == QLabel:
                if chooser == status_person:
                    # self.status_bar.empty_label_img.setStyleSheet('')
                    widget.setStyleSheet('background-color:#76FF03')
                    # self.status_bar.safe_label_img.setStyleSheet('background-color:#76FF03')
                else:
                    widget.setStyleSheet('background-color:#A7C0CD')

                chooser +=1

        # if not self.motion_empty_tick:
        #     self.status_bar.empty_label_img.setStyleSheet('background-color:#FFEB3B')
        #     self.motion_empty_tick = True
        # if motion.y() < 25 and not self.motion_tick:
        #     self.motion_tick = True
        #
        # bandwidth = len(self.audio_status.classes)
        # audio_signal_y = audio_signal.y()
        # int_audio_signal_y = abs(int(audio_signal_y * bandwidth * 10))
        #
        # if int_audio_signal_y == 3 and not self.audio_tick and self.motion_tick:
        #     self.audio_tick = True
        #     chooser = 0
        #     for widget in self.audio_status.children():
        #         if type(widget) == QLabel:
        #             if chooser == int_audio_signal_y:
        #                 self.status_bar.empty_label_img.setStyleSheet('')
        #                 widget.setStyleSheet('background-color:#76FF03')
        #                 self.status_bar.safe_label_img.setStyleSheet('background-color:#76FF03')
        #
        #             chooser += 1

    @Slot()
    def exit_app(self):
        self.status_timer.stop()
        sys.exit()


if __name__ == '__main__':
    input_file_path = '/home/parstech/PycharmProjects/multimodal_dl/sensors-data.csv'
    app = QApplication(sys.argv)
    window = MainWindow(input_file_path)
    window.show()
    sys.exit(app.exec())
