from PySide6.QtGui import *
from PySide6.QtWidgets import *
from PySide6.QtCore import *


class Status(QFrame):
    def __init__(self, icon_path, name):
        super(Status, self).__init__()
        self.size = QSize(50, 50)
        self._icon_image = QPixmap(icon_path).scaled(self.size)
        self.label_image = QLabel()
        self.label_image.setPixmap(self._icon_image)
        self.label_image.setMaximumSize(self.size)

        self.label_text = QLabel(name)
        self.label_text.setAlignment(Qt.AlignCenter)

        self._layout = QHBoxLayout()
        self._layout.addWidget(self.label_text)
        self._layout.addWidget(self.label_image)
        self.setLayout(self._layout)
        self.setFrameShape(QFrame.Panel)
        self.setFrameShadow(QFrame.Sunken)


class MultipleStatus(QWidget):
    def __init__(self):
        super(MultipleStatus, self).__init__()
        self.status1 = Status('images/verify.png', "Activity")
        self.status2 = Status('images/blank-house.png', "No Activity")

        self._layout = QHBoxLayout()
        self._layout.addItem(QSpacerItem(200, 0))
        self._layout.addWidget(self.status1)
        self._layout.addItem(QSpacerItem(200, 0))
        self._layout.addWidget(self.status2)
        self._layout.addItem(QSpacerItem(200, 0))
        self.setLayout(self._layout)


class AudioStatus(QWidget):
    def __init__(self):
        super(AudioStatus, self).__init__()
        self.size = QSize(10, 30)
        self.classes = ['FRONT', 'BACK', 'RIGHT', 'LEFT', 'other']
        _v_layout = QHBoxLayout()
        _v_layout.addItem(QSpacerItem(0, 100, QSizePolicy.Preferred, QSizePolicy.Preferred))
        for c in self.classes:
            _label = QLabel()
            _label.setMinimumSize(self.size)
            _label.setText(c)
            _label.setAlignment(Qt.AlignCenter)
            _label.setFrameShape(QFrame.Panel)
            _label.setFrameShadow(QFrame.Raised)
            if "other" in c:
                _label.setStyleSheet('background-color:#FFE082')
            else:
                _label.setStyleSheet('background-color:#A7C0CD')

            _v_layout.addWidget(_label)

        self.setLayout(_v_layout)
