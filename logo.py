from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
import sys
import time


class ImageWidget(QLabel):
    def __init__(self, path):
        super(ImageWidget, self).__init__()
        self.height = 200

        # self.setFrameShape(QFrame.Panel)
        # self.setFrameShadow(QFrame.Plain)
        # self.setMinimumWidth(self.height)
        self.image = QPixmap(path).scaledToWidth(self.height)
        # self.setPixmap(self.image)
        # self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)


class SetLogo(QWidget):
    def __init__(self):
        super(SetLogo, self).__init__()

        # _layout = QVBoxLayout()
        # logo1 = ImageWidget('logo/sensifai.png')
        # logo2 = ImageWidget('logo/Certh-logo.png')
        # logo3 = ImageWidget('logo/reach.jpg')

        self.label = QLabel(self)
        self.width = 140

        self.setMinimumWidth(self.width)
        self.image = QPixmap('logo/joy.jpg').scaledToWidth(self.width)
        self.label.setPixmap(self.image)
        self.label.setAlignment(Qt.AlignCenter)
        # self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

    def update(self, pic):
        pixmap = QPixmap(pic).scaledToHeight(self.width)
        self.label.setPixmap(pixmap)
        self.label.setAlignment(Qt.AlignCenter)
        # self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        # im_list = [logo1, logo2,logo3]
        # for i in range(3):
        #     _layout = QLabel.setPixmap(im_list[i])
            # _layout.addWidget(im_list[i])
            # _layout.addWidget(logo2)
            # _layout.addWidget(logo3)
        # _layout.setSpacing(5)
        # _layout.setContentsMargins(5, 5, 5, 5)

        # self.setLayout(_layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    widget = SetLogo()
    widget.show()
    sys.exit(app.exec())
