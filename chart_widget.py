from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from PySide6.QtCharts import *
from worker import LoadData, LoadAudio


class ChartWidget(QWidget):
    def __init__(self, data_path, name, icon_path='', column=0):
        super().__init__()
        self.size = QSize(50, 50)

        # Creating QChart
        self.icon_path = icon_path
        self.column = column
        # self.icon_pixmap = QPixmap(self.icon_path).scaled(self.size)
        # self.icon_label = QLabel()
        # self.icon_label.setPixmap(self.icon_pixmap)
        # self.icon_label.setMaximumSize(self.size)

        self.chart = QChart()
        # self.chart.setAnimationOptions(QChart.AllAnimations)
        self.name = name
        self.series = QLineSeries()
        # self.series2 = QLineSeries()

        self.chart.addSeries(self.series)
        # self.chart.addSeries(self.series2)

        self.chart.setTitle(self.name)
        # self.chart.createDefaultAxes()
        self.chart.legend().hide()
        # self.chart.axisX(self.series).setVisible(False)
        self.make_chart(self.column)
        self.load_data = LoadAudio(data_path, column=self.column)
        self.load_data.result.connect(self.add_series)
        self.load_data.start()

        # Creating QChartView
        self.chart_view = QChartView(self.chart)
        self.chart_view.setRenderHint(QPainter.Antialiasing)

        # QWidget Layout
        self.main_layout = QHBoxLayout()
        size = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        # Center layout
        self.chart_view.setSizePolicy(size)
        # self.main_layout.addWidget(self.icon_label, 1)
        self.main_layout.addWidget(self.chart_view, 10)
        # self.main_layout.setContentsMargins(0, 0, 0, 0)

        # Set the layout to the QWidget
        self.setLayout(self.main_layout)
        # self.setContentsMargins(0, 0, 0, 0)

    def add_series(self, x, y):
        self.chart.removeSeries(self.series)
        # self.chart.removeSeries(self.series2)

        # self.series2.append(QPointF(x/2, y/2))
        self.series.append(QPointF(x, y))

        if len(self.series.pointsVector()) > 400:
            # self.series.pointsVector().pop(0)
            self.series.remove(0)
        # if len(self.series2.pointsVector()) > 400:
        #     # self.series.pointsVector().pop(0)
        #     self.series2.remove(0)

        # self.series2.setColor('green')

        self.chart.addSeries(self.series)
        # self.chart.addSeries(self.series2)

    def make_chart(self, data):
        # Setting X-axis
        self.axis_x = QValueAxis()  # QDateTimeAxis()
        self.axis_x.setTickCount(5)
        # self.axis_x.setFormat("dd.MM (h:mm)")
        self.axis_x.setTitleText("Time")
        self.axis_x.setMin(-1)
        self.axis_x.setMax(1)
        self.chart.addAxis(self.axis_x, Qt.AlignBottom)
        self.series.attachAxis(self.axis_x)
        # self.series2.attachAxis(self.axis_x)

        # Setting Y-axis
        self.axis_y = QValueAxis()
        self.axis_y.setTickCount(10)
        self.axis_y.setLabelFormat("%.2f")
        self.axis_y.setTitleText("Magnitude")
        self.axis_y.setMin(-1)
        self.axis_y.setMax(1)
        self.chart.addAxis(self.axis_y, Qt.AlignLeft)
        self.series.attachAxis(self.axis_y)
        # self.series2.attachAxis(self.axis_y)


class ChartLayout(QWidget):
    def __init__(self):
        super().__init__()
        self.chart_widget = ChartWidget('sensors-data.csv', "Sensor 1", 'images/sensor1.png', column=0)
        self.chart_widget1 = ChartWidget('sensors-data.csv', 'Sensor 2', 'images/sensor1.png', column=1)
        self.chart_widget2 = ChartWidget('sensors-data.csv', 'Sensor 3', 'images/sensor1.png', column=2)
        self.chart_widget3 = ChartWidget('sensors-data.csv', 'Sensor 4', 'images/sensor1.png', column=3)

        # Charts Layout

        self.charts_layout = QVBoxLayout()
        self.charts_layout.addWidget(self.chart_widget)
        self.charts_layout.addWidget(self.chart_widget1)
        self.charts_layout.addWidget(self.chart_widget2)
        self.charts_layout.addWidget(self.chart_widget3)
        # self.charts_layout.setSpacing(0)
        self.setLayout(self.charts_layout)
        # self.charts_layout.setContentsMargins(0, 0, 0, 0)
